package com.gudkov.loopme.advservice.configuration;

import java.security.SecureRandom;
import java.util.Random;

import com.gudkov.loopme.advservice.service.AdvertisementService;
import com.gudkov.loopme.advservice.service.LocalCacheService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationContext {

    @Bean
    public AdvertisementService advertisementService(LocalCacheService localCacheService) {
        return new AdvertisementService(localCacheService);
    }

    @Bean
    public Random random() {
        return new SecureRandom();
    }
}