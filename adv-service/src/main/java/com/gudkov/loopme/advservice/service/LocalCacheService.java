package com.gudkov.loopme.advservice.service;

import java.util.Optional;

import com.gudkov.loopme.advservice.cache.CacheLoaderProperties;
import com.gudkov.loopme.advservice.cache.LocalCacheHolder;
import com.gudkov.loopme.advservice.configuration.CacheConfiguration;
import com.gudkov.loopme.model.AdvertisementModel;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestOperations;

@Log4j2
@AllArgsConstructor
public class LocalCacheService {

    private final String externalServiceUrl;
    private final AsyncRestOperations asyncRestOperations;
    private final LocalCacheHolder localCacheHolder;


    /**
     * Method try to get data from cache
     * In case of absent data initialize loading it from remote service.
     * <p>
     * @param key - specific key for data
     * @return Optional<AdvertisementModel> in case absent Optional.ofNullable.
     */
    public Optional<AdvertisementModel> tryToGetValueFromCache(String key) {

        AdvertisementModel valueFromCache = localCacheHolder.getValueFromCache(key);

        if (valueFromCache == null) {
            loadValueToCache(key);
        }

        return Optional.ofNullable(valueFromCache);
    }

    /**
     * Method try to load data by key from remote service using AsyncRestOperations
     * Limit of queue request can be configure via properties.
     * In case limit reached new request silently discards.
     * <p>
     * Configuration can be changed via
     * {@link CacheConfiguration#asyncListenableTaskExecutor(CacheLoaderProperties)}
     *
     * @param advertisementId special adv id. Should be exist on external service;
     */
    private void loadValueToCache(String advertisementId) {
        // In case we have disconnect or long request this call can be wrapped CircuitBreaker pattern.
        ListenableFuture<ResponseEntity<AdvertisementModel>> operationsForEntity = asyncRestOperations.getForEntity(buildRequestUrl(advertisementId), AdvertisementModel.class);
        operationsForEntity.addCallback(new ListenableFutureCallback<ResponseEntity<AdvertisementModel>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.error("[Fail] Getting data from remote service", throwable);
            }

            @Override
            public void onSuccess(ResponseEntity<AdvertisementModel> entity) {
                log.info("[Success] Response json: " + entity);
                AdvertisementModel model = entity.getBody();
                localCacheHolder.addValueToCache(model);
                log.info("[Success] Save model to cache: " + model);
            }
        });
    }

    private String buildRequestUrl(String advertisementId) {
        return externalServiceUrl + "?advertisementId=" + advertisementId;
    }
}