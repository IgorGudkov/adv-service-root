package com.gudkov.loopme.advservice.configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.gudkov.loopme.advservice.cache.CacheHolderProperties;
import com.gudkov.loopme.advservice.cache.CacheLoaderProperties;
import com.gudkov.loopme.advservice.cache.LocalCacheHolder;
import com.gudkov.loopme.advservice.service.LocalCacheService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.web.client.AsyncRestOperations;
import org.springframework.web.client.AsyncRestTemplate;

@Configuration
public class CacheConfiguration {

    @Value("${service.backend.url}")
    private String externalServiceUrl;

    @Bean
    @ConfigurationProperties("service.cache.holder")
    public CacheHolderProperties cacheHolderProperties() {
        return new CacheHolderProperties();
    }

    @Bean
    @ConfigurationProperties("service.cache.loader.threadPool")
    public CacheLoaderProperties cacheLoaderProperties() {
        return new CacheLoaderProperties();
    }

    @Bean(initMethod = "init")
    public LocalCacheHolder localCache(CacheHolderProperties cacheHolderProperties) {
        return new LocalCacheHolder(cacheHolderProperties);
    }

    @Bean
    public LocalCacheService localCacheService(AsyncRestOperations asyncRestOperations, LocalCacheHolder localCacheHolder) {
        return new LocalCacheService(externalServiceUrl, asyncRestOperations, localCacheHolder);
    }

    @Bean
    public AsyncRestOperations asyncRestOperations(CacheLoaderProperties cacheLoaderProperties) {
        return new AsyncRestTemplate(asyncListenableTaskExecutor(cacheLoaderProperties));
    }

    @Bean
    public AsyncListenableTaskExecutor asyncListenableTaskExecutor(CacheLoaderProperties cacheLoaderProperties) {
        Executor executor = new ThreadPoolExecutor(
                cacheLoaderProperties.getCorePoolSize(),
                cacheLoaderProperties.getMaximumPoolSize(),
                cacheLoaderProperties.getKeepAliveTimeInSeconds(), TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(cacheLoaderProperties.getQueueCapacity()), new ThreadPoolExecutor.DiscardPolicy());
        return new ConcurrentTaskExecutor(executor);
    }
}