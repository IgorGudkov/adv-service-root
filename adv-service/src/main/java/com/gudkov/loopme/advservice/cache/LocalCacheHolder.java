package com.gudkov.loopme.advservice.cache;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.gudkov.loopme.model.AdvertisementModel;

import static com.gudkov.loopme.utils.KeyUtil.buildCacheKey;


/**
 * Local cache holder, provide quick access to data.
 * Work only for one instance of application.
 * In case multiple instance of app. Better to use
 * something like memcached/couchbase or redis.
 */
public class LocalCacheHolder {

    private final CacheHolderProperties cacheHolderProperties;
    private Cache<String, AdvertisementModel> cacheHolder;

    public LocalCacheHolder(CacheHolderProperties cacheHolderProperties) {
        this.cacheHolderProperties = cacheHolderProperties;
    }

    public void init() {
        this.cacheHolder = CacheBuilder.newBuilder()
                .concurrencyLevel(cacheHolderProperties.getConcurrencyLevel())
                .maximumSize(cacheHolderProperties.getMaximumSize())
                .expireAfterAccess(cacheHolderProperties.getExpireDurationSeconds(), TimeUnit.SECONDS)
                .build();
    }

    public AdvertisementModel getValueFromCache(String key) {
        return cacheHolder.getIfPresent(key);
    }

    public void addValueToCache(AdvertisementModel value) {
        cacheHolder.put(buildCacheKey(value.getId()), value);
    }
}