package com.gudkov.loopme.advservice.service;

import java.util.Optional;

import com.gudkov.loopme.model.AdvertisementModel;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AdvertisementService {

    private final LocalCacheService cacheService;

    public Optional<AdvertisementModel> getAdvertisementData(String advertisementId) {
        return cacheService.tryToGetValueFromCache(advertisementId);
    }
}