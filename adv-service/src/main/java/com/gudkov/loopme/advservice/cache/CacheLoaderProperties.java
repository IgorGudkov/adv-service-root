package com.gudkov.loopme.advservice.cache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CacheLoaderProperties {

    private int corePoolSize;
    private int maximumPoolSize;
    private int keepAliveTimeInSeconds;
    private int queueCapacity;
}