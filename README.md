**Components:**

1. adv-service : our service that communicate with client
2. adv-service-external : service generate some data with keys between [0,1000000]
3. adv-common : lib that contains models and keyGeneranor

**How to run:**

1. Build project by maven : "mvn clean install"
2. Run external service : "java -jar /{path}/adv-service-external-0.0.1-SNAPSHOT.jar"
3. Run test service : "java -jar /{path}/adv-service-0.0.1-SNAPSHOT.jar"

**Test benchmarks:**

| Server Software | CPU: Intel(R) Core(TM) i5-6440HQ CPU @ 2.60GHz, Memory: 16Gb |
|-----------------|--------------------------------------------------------------|
| Server Hostname | localhost                                                    |
| Server Port     | 8855                                                         |
| Document Path   | adv-service/adv                                              |

[1 run] ab -n 100000 -c 1000 http://localhost:8855/adv-service/adv

| Concurrency Level    | 1000                                              |
|----------------------|---------------------------------------------------|
| Time taken for tests | 5.543 seconds                                     |
| Complete requests    | 100000                                            |
| Failed requests      | 11797                                             |
| Total transferred    | 9987715 bytes                                     |
| HTML transferred     | 469196 bytes                                      |
| Requests per second  | 18039.42 [#/sec] (mean)                           |
| Time per request     | 55.434 [ms] (mean)                                |
| Time per request     | 0.055 [ms] (mean, across all concurrent requests) |
| Transfer rate        | 1759.50 [Kbytes/sec] received                     |


| Percentage of the requests served within a certain time | (ms)                   |
|---------------------------------------------------------|------------------------|
| 50%                                                     | 5                      |
| 66%                                                     | 7                      |
| 75%                                                     | 8                      |
| 80%                                                     | 8                      |
| 90%                                                     | 12                     |
| 95%                                                     | 52                     |
| 98%                                                     | 1006                   |
| 99%                                                     | 1012                   |
| 100%                                                    | 3015 (longest request) |


[2 run] ab -n 100000 -c 1000 http://localhost:8855/adv-service/adv

| Concurrency Level    | 1000                                              |
|----------------------|---------------------------------------------------|
| Time taken for tests | 4.673 seconds                                     |
| Complete requests    | 100000                                            |
| Failed requests      | 13153                                             |
| Total transferred    | 10078315 bytes                                    |
| HTML transferred     | 469196 bytes                                      |
| Requests per second  | 21401.03 [#/sec] (mean                            |
| Time per request     | 46.727 [ms] (mean)                                |
| Time per request     | 0.047 [ms] (mean, across all concurrent requests) |
| Transfer rate        | 2106.31 [Kbytes/sec] received                     |


| Percentage of the requests served within a certain time | (ms)                   |
|---------------------------------------------------------|------------------------|
| 50%                                                     | 4                      |
| 66%                                                     | 5                      |
| 75%                                                     | 6                      |
| 80%                                                     | 7                      |
| 90%                                                     | 9                      |
| 95%                                                     | 14                     |
| 98%                                                     | 1006                   |
| 99%                                                     | 1015                   |
| 100%                                                    | 3021 (longest request) |


[3 run] ab -n 100000 -c 1000 http://localhost:8855/adv-service/adv

| Concurrency Level    | 1000                                              |
|----------------------|---------------------------------------------------|
| Time taken for tests | 4.673 seconds                                     |
| Complete requests    | 100000                                            |
| Failed requests      | 14564                                             |
| Total transferred    | 10172462 bytes                                    |
| HTML transferred     | 579234 bytes                                      |
| Requests per second  | 21973.64 [#/sec] (mean)                           |
| Time per request     | 45.509 [ms] (mean)                                |
| Time per request     | 0.046 [ms] (mean, across all concurrent requests) |
| Transfer rate        | 2182.87 [Kbytes/sec] received                     |


| Percentage of the requests served within a certain time | (ms)                   |
|---------------------------------------------------------|------------------------|
| 50%                                                     | 4                      |
| 66%                                                     | 5                      |
| 75%                                                     | 6                      |
| 80%                                                     | 7                      |
| 90%                                                     | 9                      |
| 95%                                                     | 22                     |
| 98%                                                     | 1006                   |
| 99%                                                     | 1010                   |
| 100%                                                    | 3206 (longest request) |
