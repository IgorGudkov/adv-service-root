package com.gudkov.loopme.advservice.cache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CacheHolderProperties {

    private int concurrencyLevel;
    private int maximumSize;
    private int expireDurationSeconds;
}