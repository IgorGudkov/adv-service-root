package com.gudkov.loopme.advservice.rest;

import java.util.Random;

import com.gudkov.loopme.advservice.service.AdvertisementService;
import com.gudkov.loopme.model.AdvertisementModel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.gudkov.loopme.utils.KeyUtil.buildCacheKey;

@RestController
@AllArgsConstructor
public class AdvertisementController {

    private static final int MAX_KEY_ID = 100_000;

    private final AdvertisementService advertisementService;
    private final Random random;

    /**
     * Endpoint provide adv data.
     * Key generates randomly between [0;100000]
     *
     * @return AdvertisementModel or null in case data not find in cache
     */
    @RequestMapping("/adv")
    public AdvertisementModel getAdvertisement() {
        return advertisementService.getAdvertisementData(getRandomKey()).orElse(null);
    }


    /**
     * Endpoint provide adv data by id.
     * Key generates randomly between [0;100000]
     *
     * @param id - specific if for adv data
     * @return AdvertisementModel or null in case data not find in cache
     */
    @RequestMapping("/adv/{id}")
    public AdvertisementModel getAdvertisement(@PathVariable long id) {
        return advertisementService.getAdvertisementData(buildCacheKey(id)).orElse(null);
    }

    private String getRandomKey() {
        int randomValue = random.nextInt(MAX_KEY_ID);
        return buildCacheKey(randomValue);
    }
}