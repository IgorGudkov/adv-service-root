package com.gudkov.loopme.advservice.rest;

import java.util.Random;

import com.gudkov.loopme.advservice.service.AdvertisementDataService;
import com.gudkov.loopme.model.AdvertisementModel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ExternalAdvertisementController {

    private static final int LOW_DELAY_MILLIS = 20;
    private static final int HIGH_DELAY_MILLIS = 2000;

    private final AdvertisementDataService advertisementDataService;
    private final Random random;

    @RequestMapping("/advertisement")
    public AdvertisementModel getRandomAdvertisementList(@RequestParam String advertisementId) throws InterruptedException {
        int delay = random.nextInt(HIGH_DELAY_MILLIS - LOW_DELAY_MILLIS) + LOW_DELAY_MILLIS;
        Thread.sleep(delay);
        return advertisementDataService.getAdvertisementData(advertisementId);
    }
}