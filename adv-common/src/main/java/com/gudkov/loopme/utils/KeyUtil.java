package com.gudkov.loopme.utils;

public class KeyUtil {

    private static final String KEY_PREFIX = "key";

    public static String buildCacheKey(long id) {
        return KEY_PREFIX + id;
    }
}
