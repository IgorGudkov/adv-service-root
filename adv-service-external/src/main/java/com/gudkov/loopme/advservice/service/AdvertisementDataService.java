package com.gudkov.loopme.advservice.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.gudkov.loopme.model.AdvertisementModel;

public class AdvertisementDataService {

    private static Map<String, AdvertisementModel> advertisementData = new ConcurrentHashMap<>();
    private static int ADV_SIZE = 100_000;

    public void init() {
        for (int id = 0; id < ADV_SIZE; id++) {
            String key = "key" + id;
            advertisementData.put(key, AdvertisementModel.builder().id(id).content("Content - " + id).build());
        }
    }

    public AdvertisementModel getAdvertisementData(String key) {
        return advertisementData.get(key);
    }
}