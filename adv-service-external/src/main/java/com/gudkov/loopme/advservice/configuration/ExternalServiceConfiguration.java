package com.gudkov.loopme.advservice.configuration;

import java.security.SecureRandom;
import java.util.Random;

import com.gudkov.loopme.advservice.rest.ExternalAdvertisementController;
import com.gudkov.loopme.advservice.service.AdvertisementDataService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExternalServiceConfiguration {

    @Bean(initMethod = "init")
    public AdvertisementDataService advertisementDataService() {
        return new AdvertisementDataService();
    }

    @Bean
    public ExternalAdvertisementController externalAdvertisementController(AdvertisementDataService advertisementDataService,
                                                                           Random random) {
        return new ExternalAdvertisementController(advertisementDataService, random);
    }

    @Bean
    public Random random() {
        return new SecureRandom();
    }
}